
package numero.palindromo;

import java.util.Scanner;


public class PalabraPalindromo {

    
    public static void main(String[] args) {
        
     
        Scanner sc = new Scanner(System.in);
        
        String  Palabra; 
        System.out.println("Ingrese una Palabra:");
        Palabra=sc.nextLine();
        
        System.out.println("La palabra-"+ Palabra+ "- es Palindromoda?:"
                
        + Palindromo(Palabra));
        
        
        
    }
    
    public static boolean Palindromo(String Palabra){
    String palabra = String.valueOf(Palabra);
    
    
    
    // se desplaza de izquierda a derecha
    for (int i = 0, j = palabra.length() -1; i <= j; i++, --j){
     if (palabra.charAt(i) != palabra.charAt(j)) {
         
     return false;
     }
    }
    return true;
    }
    
}
